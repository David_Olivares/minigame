using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadlyObj : MonoBehaviour
{
    private float[] probobjects = { 50, 50 };
    [SerializeField]private GameObject[] gameObjects;
    [SerializeField] private GameObject game;
    [SerializeField] private GameObject game1;
    private Vector3 increment = new Vector3(0, 1.5f, 0);
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnDeadly());

    }
    IEnumerator SpawnDeadly()
    {
        yield return new WaitForSeconds(3.5f);
        CreateDeadly();
        StartCoroutine(SpawnDeadly());
    }
    private void CreateDeadly()
    {
        
        float probPlat = ProbabilitySystem(probobjects);
        switch (probPlat)
        {
            case 0:
                Instantiate(game, transform.position, transform.rotation);
                break;
            case 1:
                Instantiate(game1,  transform.position + increment, transform.rotation);
                break;
        }
        
 
    }
    float ProbabilitySystem(float[] probs)
    {

        float total = 0;

        foreach (float elem in probs)
        {
            total += elem;
        }

        float randomPoint = Random.value * total;

        for (int i = 0; i < probs.Length; i++)
        {
            if (randomPoint < probs[i])
            {
                return i;
            }
            else
            {
                randomPoint -= probs[i];
            }
        }
        return probs.Length - 1;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Muerto player");
        }
    }

}
