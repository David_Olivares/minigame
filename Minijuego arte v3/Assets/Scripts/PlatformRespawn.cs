using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformRespawn : MonoBehaviour
{
    private float[] platProbs = { 25, 25, 25,25 };
    [SerializeField] private GameObject platform;
    [SerializeField] private Transform objective;
    [SerializeField] private Transform objective1;
    [SerializeField] private Transform objective2;
    [SerializeField] private Transform objective3;
    [SerializeField] private Transform final;
    GameObject platformInstantiate = null;
    public int respawn;
    private bool spawnNumber = false;
    private bool spawnNumber1 = false;
    private bool spawnNumber2 = false;
    private bool spawnNumber3 = false;
    private Transform ob;
    private Vector2 respawnInc;
    private GeneralMovement script;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartPawn());
    }

        // Update is called once per frame
    void Update()
    {

            

    }

    IEnumerator StartPawn()
    {
            if (respawn < 10)
            {
                int numPlats;
                numPlats = Random.Range(2, 4);
                for (int i = 0; i < numPlats; i++)
                {
                    float probPlat = ProbabilitySystem(platProbs);


                    switch (probPlat)
                    {
                        case 0:

                            spawnNumber = true;

                            //platformInstantiate = platform;
                            /*
                            actualPlats++;*/
                            break;
                        case 1:

                            spawnNumber1 = true;

                            //platformInstantiate = extra;
                            /*
                            actualPlats++;   */
                            break;
                        case 2:

                            spawnNumber2 = true;

                            //platformInstantiate = extra;
                            /*
                            actualPlats++;*/
                            break;
                        case 3:

                            spawnNumber3 = true;

                            //platformInstantiate = extra;
                            /*
                                actualPlats++;*/
                            break;
                    }
                    if (spawnNumber)
                    {
                        
                            Instantiate(platform, objective.position, transform.rotation);

                        spawnNumber = false;
                    }
                    else if (spawnNumber1)
                    {

                            Instantiate(platform, objective1.position, transform.rotation);

                        
                        spawnNumber1 = false;
                    }
                    else if (spawnNumber2)
                    {

                            Instantiate(platform, objective2.position, transform.rotation);

                        spawnNumber2 = false;
                    }
                    else if (spawnNumber3)
                    {

                            Instantiate(platform, objective3.position, transform.rotation);

                        spawnNumber3 = false;
                    }
                }
                respawn++;
                float distance = -8f;
                Vector3 respawnPosition = transform.position;
                respawnPosition.x -= distance;
                transform.position = respawnPosition;

                yield return new WaitForSeconds(2f);
            }
            if (respawn < 10)
            {
                StartCoroutine(StartPawn());
            }
            else
            {
                Instantiate(final, transform.position, transform.rotation);
            }

        }

    private void CreatePlatform()
    {
        Instantiate(platformInstantiate, transform.position, transform.rotation);
    }
    float ProbabilitySystem(float[] probs)
    {

        float total = 0;

        foreach (float elem in probs)
        {
            total += elem;
        }

        float randomPoint = Random.value * total;

        for (int i = 0; i < probs.Length; i++)
        {
            if (randomPoint < probs[i])
            {
                return i;
            }
            else
            {
                randomPoint -= probs[i];
            }
        }
        return probs.Length - 1;
    }
}
